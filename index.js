const sites = require('./resources/sites1')
const siteConfiguration = require('./builders/SiteConfiguration').siteConfiguration;
const livePersonHelper = require("./helpers/LivePersonHelper").LivePersonHelper;

( async() => {
    
    let results = [];

    for(let site of sites){
    
        let headers = await livePersonHelper.logIntoLiveEngage(site.id);
  
        let result  = await livePersonHelper.verifyMavenSkillExistence(site,headers);
         
        results.push(result);
    }
     
    console.log(JSON.stringify(results));
})();

