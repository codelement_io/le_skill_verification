class SmartConnectorConfiguration{

    constructor(siteId, agentloginClientId, agentloginClientSecret, botUserName, appKey, secret,accessToken,accessTokenSecret){
        this.siteId = siteId;
        this.agentloginClientId = agentloginClientId;
        this.agentloginClientSecret = agentloginClientSecret;
        this.botUserName = botUserName;
        this.appKey = appKey;
        this.secret = secret;
        this.accessToken = accessToken;
        this.accessTokenSecret = accessTokenSecret;
    }
}

module.exports = {
    SmartConnectorConfiguration
}