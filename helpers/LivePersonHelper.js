require('dotenv').config()
const puppeteer = require('puppeteer');
const axios = require('axios');
const uHelper = require("./UrlHelper").UrlHelper;
const service = require("../resources/services");
const User = require("../model/User").User;
const requestHelper = require("./RequestHelper").RequestHelper;

const LivePersonHelper = {

    logIntoLiveEngage: (siteId) => {

     console.log("siteId",siteId);
     return axios.get(uHelper.getDomainUrl(siteId, service.agentVep))
             .then( response => {
                let domain = response.data.baseURI;
          
               return axios.post(uHelper.getLoginUrl(domain, siteId), new User(process.env.LPA_USER_NAME, process.env.PASSWORD))
                          .then( response => {

                            token = response.data.bearer;
                            userId = response.data.config.userId;
                            headers = requestHelper.getAuthHeaderBySite(token, userId, siteId, '-1');

                            return headers;

                         })
                         .catch( error => {
                             console.log(error);
                         })
             })
             
    },

    grantLPAjspAdminPermissions: (siteId) => {

      return  axios.get(uHelper.getDomainUrl(siteId, service.openPlatform))
                    .then( response => {
                        return response.data.baseURI;
                    })
                    .then( domain => {

                        let jspAdminUrl    = uHelper.getJspAdminUrl(siteId, domain);
                        let userName       = process.env.USER_NAME;
                        let passWord       = process.env.PASSWORD;
                            
                        return  LivePersonHelper.authenticateJspAdmin(siteId, jspAdminUrl, userName, passWord);

                        //  return siteId;
                    })
                    .catch(error =>{
                        console.log("error",error)
                    })
        
     },

     verifyMavenSkillExistence: (site, headers) => {
 
        let siteId = site["LE Account Config"];
        let name = site["VODAFONE MAAP"];

       return axios.get(uHelper.getDomainUrl(siteId, service.accountConfigReadWrite))
                   .then( response => { 
                        return response.data.baseURI;
                   })
                   .then(readWriteDomain => {
                        domain = readWriteDomain;
                        return axios.get(uHelper.getAllSkillsUrl(readWriteDomain, siteId),{headers: headers})
                    })
                    .then(skills => {
                        
                        return skills.data.find(skill => skill.name == "maven")
                    })
                    .then( skillExists => {
                        
                        if(skillExists){
                            // console.log("Existing Maven Skill was found on siteId ["+siteId+"] with Id: "+skillExists.id);
                            skill = skillExists;
    
                           return axios.get(uHelper.getAllUsersUrl(domain, siteId),{headers: headers})
                                       .then(response => {

                                            let users = response.data;
                                             agentWithMavenSkill = false;
                                             numberOfAgents = 0;
                                            users.forEach(user => {
                                                user.skillIds.forEach(skillId => {

                                                    if(skillId == skillExists.id){
                                                        agentWithMavenSkill = true;
                                                        numberOfAgents++;
                                                        // console.log({siteId: siteId, hasMavenSkill: true, agent: user.loginName});
                                                    }
                                                    
                                                })
                                            })
                                            
                                            if(agentWithMavenSkill){
                                                return {"account_name": name, "siteId": siteId, "agents_in_maven": numberOfAgents};
                                            }else{
                                                return {"account_name": name, "siteId": siteId, "agents_in_maven": 0};
                                            }
                                       })
                                       .catch( error => {
                                           console.log("error at agent search ", error);
                                       })
                        }else{
                            console.log("doesn't exists")
                        }
                        
                    })
                    .catch( error => {
                        console.log("error",error);
                        return error;
                    })
                                          
     },

     getSiteSkillRules: (siteId, serviceId, skillId, headers) => {
                
        return axios.get(uHelper.getDomainUrl(siteId, service.accountConfigReadOnly))
                    .then( response => {
                        return  axios.get(uHelper.getAllSkillRulesUrl(siteId, response.data.baseURI),{headers: headers})           
                    })
                    .then( response => {

                        console.log("Getting Skill Rules from siteId: "+siteId);
        
                         revisionNumber = response.headers['ac-revision'];
                         skillRules = response.data;                                                                              
                         rules = skillRules.propertyValue.value.rules;

                        if(rules && rules.length > 0){

                            skillRule = rules.find(rule => rule.description == "Vodafone RCS");

                            if(skillRule){
                                console.log("Existing Maven Skill Rule was found on siteId ["+siteId+"] with orderId: "+skillRule.orderId);
                            }else{
                                orderId = Math.max.apply(Math, rules.map(function(rule) { return rule.orderId; }));
                                newRule = requestHelper.getSingleSkillRuleRequest(skillId, (orderId + 1), serviceId);
                                rules.push(newRule);
                            }
                            console.log("if",[revisionNumber, skillRules.propertyValue.value])
                        }else{
                            skillRules = requestHelper.getFullSkillRuleRequest(skillId, 1, serviceId);
                            console.log("else",[revisionNumber, skillRules]);
                          
                        }
                       
                        return [revisionNumber, skillRules];
                    })
                    .catch( error => {
                        
                        console.log("error",error);
                        return error;
                    })
    },

    updateSiteSkillRules: (siteId, skillRules, revisionNumber,headers) => {
        
        console.log("Updating Site Skill Rules on siteId: "+siteId);

        return axios.get(uHelper.getDomainUrl(siteId, service.accountConfigReadWrite))
                    .then( response => { 

                        let readWriteDomain = response.data.baseURI;
                        headers["If-Match"]= revisionNumber;

                        return axios.put(uHelper.getSkillRuleUrl(siteId,readWriteDomain),skillRules,{headers: headers})
                                    
                    })
                    .catch( error => {
                        console.log(error);
                        return error;
                    })
    }

}

module.exports = {
    LivePersonHelper
}