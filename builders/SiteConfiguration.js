const livePersonHelper = require("../helpers/LivePersonHelper").LivePersonHelper;
const SmartConnectorConfiguration = require('../model/SmartConnectorConfiguration').SmartConnectorConfiguration;
const requestHelper = require('../helpers/RequestHelper').RequestHelper;

class SiteConfiguration{

    verifyMavenSkillExistence(site) {

        let siteId = site["LE Account Config"];
        
        livePersonHelper.logIntoLiveEngage(siteId)
                        .then(headers => {
                            livePersonHelper.verifyMavenSkillExistence(site,headers)
                        })
                        .catch( error => {
                            console.log("error", error);
                        })
    };

    updateMavenSkill() {
        
        let siteId = this.siteId;

        if(this.promise){
            this.promise = this.promise.then(() => livePersonHelper.updateMavenSkill(siteId, this.serviceId, this.skillId, this.headers))
                                       .then( ([revisionNumber, skillRules]) => {
                                               this.skillRules = skillRules;   
                                               this.revisionNumber = revisionNumber;
                                        });
        }

        return this;
    };
    saveConfiguration(){

        if(this.promise){
           return this.promise
                     .then(() => {
                        
                        console.log("Returning SmartConnector configuration on siteId: "+this.siteId);
                        //@TODO not finished yet
                    let scConfig = new SmartConnectorConfiguration(
                            this.siteId,
                            this.smartConnectorWidgetInstallationKeys.client_id,
                            this.smartConnectorWidgetInstallationKeys.client_secret,
                            "mavenbot",
                            this.smartConnectorApi.keyId,
                            this.smartConnectorApi.appSecret,
                            this.smartConnectorApi.token,
                            this.smartConnectorApi.tokenSecret
                        );

                        console.log(scConfig);
                  })
        }
       
    }
}

module.exports = {
    siteConfiguration: new SiteConfiguration()
};